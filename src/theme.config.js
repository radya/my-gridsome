export default {
  navbar: {
    menu: [
      {
        title: 'Home',
        url: '/'
      },
      {
        title: 'Archives',
        url: '/archives'
      },
      {
        title: 'About Me',
        url: '/about'
      }
    ]
  },
  author: {
    name: 'Radya',
    bio: 'Penikmat telur gulung'
  },
  social: {
    twitter: 'radyakaze',
    github: 'radyakaze',
    facebook: 'radyakaze',
    instagram: 'radyarad'
  },
  disqus: {
    enable: true,
    shortname: 'radya-1'
  }
}
