// Server API makes it possible to hook into various parts of Gridsome
// on server-side and add custom data to the GraphQL data layer.
// Learn more: https://gridsome.org/docs/server-api

// Changes here requires a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`
const removeMd = require('remove-markdown')

module.exports = function(api) {
  api.loadSource(store => {
    // Use the Data store API here: https://gridsome.org/docs/data-store-api

    const posts = store.addContentType('Post');

    posts.addSchemaField('excerpt', ({ graphql }) => ({
      type: graphql.GraphQLString,
      args: {
        pruneLength: { type: graphql.GraphQLInt, defaultValue: 140 }
      },
      resolve(node, args) {
        const value = node.excerpt ? node.excerpt : removeMd(node.content);

        return value.substr(0, args.pruneLength) + '...'
      }
    }));
  });
};
